# LINE Webtoon Insights

## Problem Definition

LINE Webtoon is the biggest web comic platform operating in Indonesia, publishing hundreds of titles from authors around the world. In the wake of increasing demand for web comic, comic publisher are looking for new talents that can produce a big-hit comic. On the other side of the market, comic artists are desperately trying to get an offer to draw comics for large platform. However, the information about which type of comic is popular at the moment is not easily accessible by artists, resulting in absence of trend knowledge in the production process of their work. 

This project aims to help comic artists by logging data from LINE Webtoon regularly, and then provide latest and historical insights to artists. The insights provided are popular genres (based on likes and subscriptions), distribution of genre, and other detailed informations.

***

## Solution Overview

![Project Diagram](docs/project-diagram.jpg)

LINE Webtoon data (comic titles, authors, subscribers, likes, etc) are publicly available in their website in web-scraping-friendly format. To obtain and store the data, a web-scraping script is executed regularly (scheduled using an orchestration tool), and then the data is stored in a cloud-based storage.

The stored data then transformed using data warehousing and query engine tools, scheduled using the same orchestration tool. Finally, the updated data are connected to a BI tool to be visualized.

The final dashboard can be accessed via https://ammarchalifah.com/webtoon-insights

### Tools
Orchestration: Airflow

Storage: Google Cloud Storage

Warehouse: BigQuery

Data Visualization: Tableau

***

## Step-by-step Guide

### Part 1: Cloud Resource Provisioning with Terraform

1. Install `gcloud` SDK, `terraform` CLI, and create a GCP project. Then, create a service account with **Storage Admin**, **Storage Pbject Admin**, and **BigQuery Admin** role. Download the JSON credential and store it on `.google/credentials/google_credential.json`. Open `01_terraform/main.tf` in a text editor, and change `dataanalyticscourse-327913` with your GCP's project id.

2. Enable IAM API and IAM Credential API in GCP.

3. Change directory to `01_terraform` by executing
```
cd 01_terraform
```

4. Initialize Terraform (set up environment and install Google provider)
```
terraform init
```
5. Plan Terraform infrastructure creation
```
terraform plan
```
6. Create new infrastructure by applying Terraform plan
```
terraform apply
```
7. Check GCP console to see newly-created resources.

### Part 2: Run Airflow to Scrape, Ingest, and Warehouse Data
1. Go to airflow directory
```
cd 02_airflow
```
Then, replace `dataanalyticscourse-327913` in `.env` file to your project ID.

2. Build Airflow docker images
```
docker-compose build
```
3. Run Airflow docker containers
```
docker-compose up
```
or for daemonized run
```
docker-compose up -d
```
4. Access the Airflow webserver through web browser on `localhost:8080`. Unpause the DAGs.
   
![Airflow Webserver UI](docs/airflow-ui.png)

![Airflow Data Ingestion DAG](docs/airflow-ingestion.png)

![Airflow Datawarehouse DAG](docs/airflow-dw.png)

5. After done, check GCP Console, both in Google Cloud Storage and BigQuery to check the data.

![BigQuery Datasets](docs/bq-dataset.png)

![BigQuery Query Result](docs/bq-query.png)

### Part 3: Visualize Data
1. Open Tableau Desktop, connect to BigQuery.
2. Visualize the dashboard, publish to Tableau Public.
3. Embed the dashboard as HTML to your website.

![Dashboard](docs/dashboard-sample.png)

### Part 4: Shutting Down the Project
1. To temporarily shut down the project, just stop the containers
```
docker-compose down
```
2. To completely remove the project, including the cloud resources:
```
# From inside 02_airflow directory
docker-compose down --volumes

# From inside 01_terraform directory
terraform destroy
```

***

## Other Resources
### Blog Post
Similar project, with GitLab CI/CD and deployment on AWS EC2: https://medium.com/towards-data-science/running-airflow-with-docker-on-ec2-ci-cd-with-gitlab-72326d4baeb4 

### Data Engineering Zoomcamp by DataTalksClub
https://github.com/DataTalksClub/data-engineering-zoomcamp