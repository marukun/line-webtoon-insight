import requests
import logging
import datetime

import pandas as pd

from bs4 import BeautifulSoup

def numeric_parser(text):
    text = text.replace(".","")
    text = text.replace(",",".")
    if 'RB' in text:
        text = int(float(text.replace("RB",""))*(10**3))
    elif 'JT' in text:
        text = int(float(text.replace('JT',''))*(10**6))
    elif 'M' in text:
        text = int(float(text.replace('M',''))*(10**9))
    else:
        text = int(float(text))
    return text

def scrape_official_webtoons(output_file):
    # set today's date
    today = datetime.date.today()

    URL = 'https://www.webtoons.com/id/genre'
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find("div", class_="card_wrap genre")

    webtoons = {'title': [],
                'author': [],
                'likes': [],
                'url':[],
                'genre':[],
                'views':[],
                'subs':[],
                'star_score':[],
                'unsuitable_for_children':[],
                'date':[],
                'title_id':[]
                }

    genre_elements = results.find_all("ul", class_="card_lst")
    for g in genre_elements:

        webtoon_elements = g.find_all("a")
        for w in webtoon_elements:
            webtoon_title = w.find("p", class_="subj").text
            webtoon_author = w.find("p", class_="author").text
            webtoon_likes = w.find("em", class_="grade_num").text

            webtoon_a_html = str(w).split('>')[0]+'/>'
            webtoon_a_soup = BeautifulSoup(webtoon_a_html, "html.parser")
            webtoon_url = webtoon_a_soup.find('a').get('href')
            webtoon_unsuitable_for_children = webtoon_a_soup.find('a').get('data-title-unsuitable-for-children')

            webtoon_title_page = requests.get(webtoon_url)
            webtoon_title_soup = BeautifulSoup(webtoon_title_page.content, "html.parser")

            webtoon_genre = webtoon_title_soup.find("h2", class_="genre").text
            webtoon_views = webtoon_title_soup.findAll("em", class_="cnt")[0].text
            webtoon_subs = webtoon_title_soup.findAll("em", class_="cnt")[1].text
            webtoon_star_score = webtoon_title_soup.find("em", class_="cnt", id="_starScoreAverage").text


            # convert numeric values
            webtoon_likes = numeric_parser(webtoon_likes)
            webtoon_views = numeric_parser(webtoon_views)
            webtoon_subs = numeric_parser(webtoon_subs)

            # Update dictionary
            webtoons['title'].append(webtoon_title)
            webtoons['author'].append(webtoon_author)
            webtoons['likes'].append(webtoon_likes)
            webtoons['unsuitable_for_children'].append(webtoon_unsuitable_for_children)
            webtoons['genre'].append(webtoon_genre)
            webtoons['url'].append(webtoon_url)
            webtoons['views'].append(webtoon_views)
            webtoons['subs'].append(webtoon_subs)
            webtoons['star_score'].append(webtoon_star_score)
            webtoons['date'].append(today)
            webtoons['title_id'].append(webtoon_url.split('=')[-1])
    
    df = pd.DataFrame.from_dict(webtoons)
    logging.info(df)
    logging.info(df.info())

    # Store data as csv
    df.to_csv(output_file, index = False)

if __name__ == '__main__':
    scrape_official_webtoons()